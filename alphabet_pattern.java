package programing;

public class alphabet_pattern {

	public static void main(String[] args) {
		a_pattern();
		b_pattern();
		c_pattern();
		k_pattern();
	}
	private static void c_pattern() {
		for(int row=1;row<=5;row++) {
			for(int col=1;col<=5;col++){
				if((row==1)&&(col==1) ) {
					System.out.print("  ");
				}
			    else if((row==5)&&(col==1) ) {
					System.out.print("  ");
				}
				else if((row==1)||(row==5)||(col==1) ){				
					System.out.print("* ");
					}
				else {
					System.out.print("");
				}
			}
			System.out.println();
		}
		System.out.println("===========");
	}

	private static void b_pattern() {
		for(int row=1;row<=5;row++) {
			for(int col=1;col<=5;col++){
				if((row==3) && (col==5)||(row==1) && (col==5)||(row==5) && (col==5)) {
					System.out.print("");
				}
				else if((row==1)||(row==3)||(row==5)||(col==1)||(col==5) ){				
					System.out.print("* ");
					}
				else {
					System.out.print("  ");
				}
			}
			System.out.println();
		}
		System.out.println("===========");
	}


	private static void a_pattern() {
		
		for(int row=1;row<=5;row++) {
			for(int col=1;col<=5;col++){
				if(((row==1) && (col==1))||((row==1) && (col==7))){
					System.out.print(" ");
				}
				else if((row==1)||(row==3) || (col==1)||(col==5) ){				
					System.out.print("* ");
					}
				else {
					System.out.print("  ");
				}
			}
			
			System.out.println();
		}
		System.out.println("===========");
	}

private static void k_pattern() {
		
		for(int row=1;row<=5;row++) {
			for(int col=1;col<=5;col++){
				 if((col==1)||(row+col==4)||(row-col==2)){				
					System.out.print("* ");
					}
				else {
					System.out.print("  ");
				}
			}
			System.out.println();
		}
		System.out.println("===========");
	}

}
